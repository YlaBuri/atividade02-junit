package br.ucsal.bes2020.testequalidade.atividade02;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CalculoVerificarPrimoTest {
	
	@Parameters(name = "{0} � primo: {1}")
	public static Collection<Object[]> obterDadosDeTeste() {
		 Collection<Object[]> casosDeTeste = new ArrayList<Object[]>();
		 casosDeTeste.add(new Object[] {7, true});
		 casosDeTeste.add(new Object[] {29, true});
		 casosDeTeste.add(new Object[] {19,true});
		 casosDeTeste.add(new Object[] {67, true});
		 casosDeTeste.add(new Object[] {83,true});
		 
		 casosDeTeste.add(new Object[] {10,false});
		 casosDeTeste.add(new Object[] {100,false});
		 casosDeTeste.add(new Object[] {28,false});
		 casosDeTeste.add(new Object[] {51,false});
		 casosDeTeste.add(new Object[] {96, false});
		 return casosDeTeste;
	}
	
	@Parameter 
	public Integer n;

	@Parameter(1)
	public Boolean ResultEsperado;
	
	@Test
	public void testarPrimo() {
		Boolean resultadoObtido = Calculo.verificarPrimo(n);
		Assert.assertEquals(ResultEsperado, resultadoObtido);
	}
	
}
