package br.ucsal.bes2020.testequalidade.atividade02;

import java.io.ByteArrayInputStream;

import org.junit.Assert;
import org.junit.Test;

public class CalculoEntradaTest {

	@Test
	public void testarEntrada() {
		ByteArrayInputStream entrada = new ByteArrayInputStream("50000\n-1\n87".getBytes());
		
		System.setIn(entrada);
		int resultadoAtual = Calculo.obterNumero();
		int resultadoEsperado = 87;
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
	}
	

}
