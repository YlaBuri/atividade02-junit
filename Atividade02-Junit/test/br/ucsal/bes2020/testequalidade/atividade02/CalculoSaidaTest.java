package br.ucsal.bes2020.testequalidade.atividade02;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.Test;

public class CalculoSaidaTest {
	@Test
	public void testarSaida20() {
		ByteArrayOutputStream saida = new ByteArrayOutputStream();
		
		System.setOut(new PrintStream(saida));
		Calculo.mostrarMensagem(20, false);
		String resultadoAtual = saida.toString();
		String resultadoEsperado = "O n�mero 20 n�o � primo\n";
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
		
	}
	
	@Test
	public void testarSaida07() {
		ByteArrayOutputStream saida = new ByteArrayOutputStream();
		
		System.setOut(new PrintStream(saida));
		Calculo.mostrarMensagem(7, true);
		String resultadoAtual = saida.toString();
		String resultadoEsperado = "O n�mero 7 � primo\n";
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
		
	}
}
