package br.ucsal.bes2020.testequalidade.atividade02;

import java.util.Scanner;

public class Calculo {
	static Scanner sc = new Scanner(System.in);

	public static Integer obterNumero() {
		Integer n;
		System.out.println("Informe um n�mero entre 1 e 10000");
		do {
			n = sc.nextInt();
			if(n > 10000 || n < 1) {
				System.out.println("N�mero inv�lido");
			}
		} while (n > 10000 || n < 1);
		return n;
	}

	public static boolean verificarPrimo(int n) {

		for (int i = 2; i < n; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}

	public static void mostrarMensagem(int n, boolean primo) {
		if (primo) {
			System.out.println("O n�mero " + n + " � primo");
		} else {
			System.out.println("O n�mero " + n + " n�o � primo");
		}
	}

}
